#include "filter.h"
#include <cstring>
#include <iostream>

description_wind** filter(description_wind* array[], int size, bool (*check)(description_wind* element), int& result_size)
{
	description_wind** result = new description_wind * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_book_subscription_by_direction(description_wind* element)
{
	return strcmp(element->direction, "West") == 0 ||
		strcmp(element->direction, "NorthWest") == 0 ||
		strcmp(element->direction, "North") == 0;
}

bool check_book_subscription_by_speed(description_wind* element)
{
	return element->speed > 5;
}
