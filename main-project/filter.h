#ifndef FILTER_H
#define FILTER_H

#include "Wind_description.h"
description_wind** filter(description_wind* array[], int size, bool (*check)(description_wind* element), int& result_size);
bool check_book_subscription_by_direction(description_wind* element);
bool check_book_subscription_by_speed(description_wind* element);
#endif