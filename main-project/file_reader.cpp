#include "file_reader.h"
#include "Constants.h"

#include <fstream>
#include <cstring>



void read(const char* wind, description_wind* array[], int& size)
{
    std::ifstream file(wind);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            description_wind* item = new description_wind;
            file >> item->DateWind.day;
            file >> item->DateWind.month;
            file >> item->direction;
            file >> item->speed;
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}