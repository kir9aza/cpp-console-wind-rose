#ifndef WIND_SUBSCRIPTION_H
#define WIND_SUBSCRIPTION_H

#include "constants.h"


struct date
{
    int day;
    int month;
};

struct description_wind
{
    char direction[MAX_STRING_SIZE];
    double speed;
    date DateWind;
};



#endif