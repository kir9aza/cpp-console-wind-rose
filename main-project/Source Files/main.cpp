#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <string.h>


using namespace std;

#include "../Constants.h"
#include "../Wind_description.h"
#include "../file_reader.h"
#include "../filter.h"
void output(description_wind* subscription)
{
    /**********   **********/
    //  
    cout << subscription->DateWind.day << " ";
    //    
    cout << subscription->DateWind.month << " ";
    //    
    cout << subscription->direction << " ";
    cout << subscription->speed << " ";
    cout << '\n';
 
}


int main()
{
    setlocale(LC_ALL, "");
    std::cout << "Hello\n";
    cout << "Laboratory work #1. GIT\n";
    cout << "Variant #6. Wind rose\n";
    cout << "Author: KIRYL BUSTROU\n";
    description_wind* description[MAX_FILE_ROWS_COUNT];

    int size;

    try
    {
        read("../wind.txt", description, size);
        for (int i = 0; i < size; i++)
        {

            cout << description[i]->DateWind.day << " ";
            cout << description[i]->DateWind.month << " ";
            cout << description[i]->direction << " ";
            cout << description[i]->speed << " ";
            cout << '\n';
        }
        bool (*check_function)(description_wind*) = NULL;
        cout << "1) ������� ��� ���, � ������� ��� ����� � ����� �� ����������� West, NorthWest ��� North." << '\n';
        cout << "2) ������� ��� ���, � ������� ��� ����� ������ 5 �/�."<< '\n';
        int item;
        cin >> item;
        cout << '\n';
        switch (item) {
        case 1: check_function = check_book_subscription_by_direction; //       
            break;

        case 2:
            check_function = check_book_subscription_by_speed; //       
            break;
        
        default:
            throw "  ";
    }
        if (check_function)
        {
            int new_size;
            description_wind** filtered = filter(description, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }


        for (int i = 0; i < size; i++)
        {
            delete description[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}

