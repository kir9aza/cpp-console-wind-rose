#ifndef FILE_READER_H
#define FILE_READER_H

#include "Wind_description.h"

void read(const char* wind, description_wind* array[], int& size);

#endif